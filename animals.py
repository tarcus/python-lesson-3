class Animal():
    def __init__(self, lifespan, name):
        self.__lifespan = lifespan
        self.name = name

    @property
    def lifespan(self):
        return self.__lifespan

    def cry(self):
        pass

    def breed(self):
        pass

    def age(self):
        pass

    def die(self):
        pass

class Cat(Animal):
    def __init__(self, lifespan, name, sex):
        super().__init__(lifespan, name)
        self.__sex = sex

    @property
    def sex(self):
        return self.__sex

    def cry(self):
        print("Meow!")

    def breed(self, mate):
        if not isinstance(mate, Cat) or mate.sex == self.sex:
            print("Wrong mate")
        else:
            print("Meow-Meow!")

    def age(self):
        print("Meow :(")

    def die(self):
        print("x_x__")

    def fight(self, enemy):
        print(f"Cat {self.name} claws {enemy.name}")

class Dog(Animal):
    def __init__(self, lifespan, name, sex):
        super().__init__(lifespan, name)
        self.__sex = sex

    @property
    def sex(self):
        return self.__sex

    def cry(self):
        print("Woof!")

    def breed(self, mate):
        if not isinstance(mate, Dog) or mate.sex == self.sex:
            print("Wrong mate")
        else:
            print("Woof-Woof!")

    def age(self):
        print("Woof :(")

    def die(self):
        print("x_x_")

    def fight(self, enemy):
        print(f"Dog {self.name} gnaws {enemy.name}")

class Bird(Animal):
    def __init__(self, lifespan, name, sex):
        super().__init__(lifespan, name)
        self.__sex = sex

    @property
    def sex(self):
        return self.__sex

    def cry(self):
        print("Tweet!")

    def breed(self, mate):
        if not isinstance(mate, Bird) or mate.sex == self.sex:
            print("Wrong mate")
        else:
            print("Tweet-Tweet!")

    def age(self):
        print("Woof :(")

    def die(self):
        print("_x_x_")

    def fight(self, enemy):
        print(f"Bird {self.name} pecks {enemy.name}")

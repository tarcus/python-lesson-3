class Controller:
    def start(self):
        pass

    def move_right(self, velocity):
        pass

    def move_left(self, velocity):
        pass

    def stop(self):
        pass


class Device:
    def __init__(self, serial, press):
        self.serial = serial
        self.press = press

    def start_press(self):
        self.press.start()

    def increase_pressure(self, delta):
        self.press.move_right(delta * 5)

    def decrease_pressure(self, delta):
        self.press.move_left(delta * 5)

    def stop_press(self):
        self.press.stop()


class Arduino(Controller):
    def start(self):
        print('start Arduino')

    def move_right(self, velocity):
        print(f'right Arduino {velocity}')

    def move_left(self, velocity):
        print(f'left Arduino {velocity}')
    
    def stop(self):
        print('stop Arduino')


class Raspberry(Controller):
    def start(self):
        print('start Raspberry')

    def move_right(self, velocity):
        print(f'right Raspberry {velocity}')

    def move_left(self, velocity):
        print(f'left Raspberry {velocity}')
    
    def stop(self):
        print('stop Raspberry')
        
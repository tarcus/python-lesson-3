from collections import namedtuple

UIM435 = namedtuple('UIM', ['start', 'stop', 'move_right', 'move_left'])

def uim_right(velocity):
    print(f'uim right {velocity}')

def uim_left(velocity):
    print(f'uim left {velocity}')

def uim_start():
    print('uim start')

def uim_stop():
    print('uim stop')

uim = UIM435(uim_start, uim_stop, uim_right, uim_left)

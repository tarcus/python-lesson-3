import animals as a
import transport as t

if __name__ == "__main__":
    garfield = a.Cat(15, 'Garfield', 'male')
    snowflake = a.Cat(15, 'Snowflake', 'female')
    rex = a.Dog(20, 'Rex', 'male')
    woody = a.Bird(10, 'Woody', 'male')

    garfield.cry()
    garfield.breed(snowflake)
    garfield.breed(rex)
    rex.fight(garfield)
    garfield.die()
    snowflake.fight(rex)
    rex.cry()
    rex.die()
    woody.cry()
    woody.fight(snowflake)
    snowflake.die()
    woody.die()

    pad = t.Launchpad()
    volvo = t.Truck(130, "Volvo truck")
    boing737 = t.Plane(2000, 'Boing-737', 10000)
    nuke = t.nuke

    pad.launch(volvo)
    pad.launch(boing737)
    pad.launch(nuke)

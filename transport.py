from collections import namedtuple

class Launchpad():
    def __init__(self, *args, **kwargs):
        pass

    def launch(self, vehicle):
        vehicle.start()
        print(f"Vehicle {vehicle.name} launched!")


class Transport():
    def __init__(self, speed, name):
        self.__speed = speed
        self.name = name

    @property
    def speed(self):
        return self.__speed

    def start(self):
        print(f"{self.name} started")


class Truck(Transport):
    def __init__(self, speed, name):
        super().__init__(speed, name)


class Plane(Transport):
    def __init__(self, speed, name, cruising_altitude):
        super().__init__(speed, name)
        self.cruising_altitude = cruising_altitude

    def start(self):
        print(f"{self.name} took off and started to climb up to {self.cruising_altitude} feet")


Nuke = namedtuple('Nuke', ['name', 'speed', 'start'])
def start():
    print("Nuke locked on target")
    print("Nuke initiated launch sequence")
    print("Nuke launched")
    print("Everyone is dead by now")

nuke = Nuke('Fat Man', 2500, start)    